package ru.itpark.service;

import ru.itpark.entity.Ipm;
import ru.itpark.entity.Worker;

import java.util.List;

public interface WorkersService {
    List<Worker> findAllWorker();

    void addWorker(Worker worker);

    List<Worker> searchWorkerByName(String name);

    Worker findWorkerById(int id);

    void removeWorkerById(int id);

    List<Ipm> findAllIpm();

    void addIpm(Ipm ipm);

    List<Ipm> searchIpmByName(String name);

    Ipm findIpmById(int id);

    void removeIpmById(int id);

    void service();

    List<Ipm> searchIpmByWorkerId(int id);

}
