package ru.itpark.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.itpark.entity.Ipm;
import ru.itpark.entity.Worker;
import ru.itpark.repository.IpmsRepository;
import ru.itpark.repository.WorkersRepository;

import java.util.List;

@Service
@Transactional
public class WorkersServiceImpl implements WorkersService {
    private final WorkersRepository workersRepository;
    private final IpmsRepository ipmsRepository;
    @Autowired
    public WorkersServiceImpl(WorkersRepository workersRepository, IpmsRepository ipmsRepository) {
        this.workersRepository = workersRepository;
        this.ipmsRepository = ipmsRepository;
    }


    @Override
    public List<Worker> findAllWorker() {
        return workersRepository.findAll();
    }


    @Override
    public void addWorker(Worker worker) {
        workersRepository.save(worker);
    }


    @Override
    public List<Worker> searchWorkerByName(String name) {
        return workersRepository.findAllByNameContainsIgnoreCase(name);
    }


    @Override
    public Worker findWorkerById(int id) {
        return workersRepository.findById(id).get();
    }


    @Override
    public void removeWorkerById(int id) {
        workersRepository.deleteById(id);
    }

    @Override
    public List<Ipm> findAllIpm() {
        return ipmsRepository.findAll();
    }


    @Override
    public void addIpm(Ipm ipm) {
        ipmsRepository.save(ipm);
    }


    @Override
    public List<Ipm> searchIpmByName(String name) {
        return ipmsRepository.findAllByNameContainsIgnoreCase(name);
    }


    @Override
    public Ipm findIpmById(int id) {
        return ipmsRepository.findById(id).get();
    }


    @Override
    public void removeIpmById(int id) {
        ipmsRepository.deleteById(id);
    }
    @Override
    public List<Ipm> searchIpmByWorkerId(int id) {
        return ipmsRepository.findAllByWorker_Id(id);
    }


    @Override
    public void service() {
        Ipm fullBodyHarnessOne = new Ipm();
        fullBodyHarnessOne.setId(0);
        fullBodyHarnessOne.setName("Страховочная привязь");
        fullBodyHarnessOne.setManufacturer("Vento");
        fullBodyHarnessOne.setSerialNumber("1312007272");
        fullBodyHarnessOne.setReleaseDate("01.04.2017");
        fullBodyHarnessOne.setInServiceDate("01.07.2017");
        fullBodyHarnessOne.setExpirationDate("01.07.2022");
        fullBodyHarnessOne.setNextPeriodicInspectionDate("01.07.2018");
        fullBodyHarnessOne.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");


        Ipm fullBodyHarnessTwo = new Ipm();
        fullBodyHarnessTwo.setId(0);
        fullBodyHarnessTwo.setName("Страховочная привязь");
        fullBodyHarnessTwo.setManufacturer("Vento");
        fullBodyHarnessTwo.setSerialNumber("1312007261");
        fullBodyHarnessTwo.setReleaseDate("01.04.2017");
        fullBodyHarnessTwo.setInServiceDate("11.06.2017");
        fullBodyHarnessTwo.setExpirationDate("11.06.2022");
        fullBodyHarnessTwo.setNextPeriodicInspectionDate("11.06.2018");
        fullBodyHarnessTwo.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm fullBodyHarnessThree = new Ipm();
        fullBodyHarnessThree.setId(0);
        fullBodyHarnessThree.setName("Страховочная привязь");
        fullBodyHarnessThree.setManufacturer("Vento");
        fullBodyHarnessThree.setSerialNumber("1312007112");
        fullBodyHarnessThree.setReleaseDate("01.04.2017");
        fullBodyHarnessThree.setInServiceDate("21.07.2017");
        fullBodyHarnessThree.setExpirationDate("21.07.2022");
        fullBodyHarnessThree.setNextPeriodicInspectionDate("21.07.2018");
        fullBodyHarnessThree.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm fullBodyHarnessFour = new Ipm();
        fullBodyHarnessFour.setId(0);
        fullBodyHarnessFour.setName("Страховочная привязь");
        fullBodyHarnessFour.setManufacturer("Vento");
        fullBodyHarnessFour.setSerialNumber("1312007218");
        fullBodyHarnessFour.setReleaseDate("01.04.2017");
        fullBodyHarnessFour.setInServiceDate("01.07.2017");
        fullBodyHarnessFour.setExpirationDate("01.07.2022");
        fullBodyHarnessFour.setNextPeriodicInspectionDate("01.07.2018");
        fullBodyHarnessFour.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm fullBodyHarnessFive = new Ipm();
        fullBodyHarnessFive.setId(0);
        fullBodyHarnessFive.setName("Страховочная привязь");
        fullBodyHarnessFive.setManufacturer("Vento");
        fullBodyHarnessFive.setSerialNumber("1312007298");
        fullBodyHarnessFive.setReleaseDate("01.04.2017");
        fullBodyHarnessFive.setInServiceDate("29.07.2017");
        fullBodyHarnessFive.setExpirationDate("29.07.2022");
        fullBodyHarnessFive.setNextPeriodicInspectionDate("29.07.2018");
        fullBodyHarnessFive.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm fullBodyHarnessSix = new Ipm();
        fullBodyHarnessSix.setId(0);
        fullBodyHarnessSix.setName("Страховочная привязь");
        fullBodyHarnessSix.setManufacturer("Vento");
        fullBodyHarnessSix.setSerialNumber("1312007354");
        fullBodyHarnessSix.setReleaseDate("01.04.2017");
        fullBodyHarnessSix.setInServiceDate("12.06.2017");
        fullBodyHarnessSix.setExpirationDate("12.06.2022");
        fullBodyHarnessSix.setNextPeriodicInspectionDate("12.06.2018");
        fullBodyHarnessSix.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm fullBodyHarnessSeven = new Ipm();
        fullBodyHarnessSeven.setId(0);
        fullBodyHarnessSeven.setName("Страховочная привязь");
        fullBodyHarnessSeven.setManufacturer("Vento");
        fullBodyHarnessSeven.setSerialNumber("1312007198");
        fullBodyHarnessSeven.setReleaseDate("01.04.2017");
        fullBodyHarnessSeven.setInServiceDate("03.07.2017");
        fullBodyHarnessSeven.setExpirationDate("03.07.2022");
        fullBodyHarnessSeven.setNextPeriodicInspectionDate("03.07.2018");
        fullBodyHarnessSeven.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm fullBodyHarnessEight = new Ipm();
        fullBodyHarnessEight.setId(0);
        fullBodyHarnessEight.setName("Страховочная привязь");
        fullBodyHarnessEight.setManufacturer("Vento");
        fullBodyHarnessEight.setSerialNumber("1312007221");
        fullBodyHarnessEight.setReleaseDate("01.04.2017");
        fullBodyHarnessEight.setInServiceDate("05.07.2017");
        fullBodyHarnessEight.setExpirationDate("05.07.2022");
        fullBodyHarnessEight.setNextPeriodicInspectionDate("05.07.2018");
        fullBodyHarnessEight.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm fullBodyHarnessNine = new Ipm();
        fullBodyHarnessNine.setId(0);
        fullBodyHarnessNine.setName("Страховочная привязь");
        fullBodyHarnessNine.setManufacturer("Vento");
        fullBodyHarnessNine.setSerialNumber("1312007253");
        fullBodyHarnessNine.setReleaseDate("01.04.2017");
        fullBodyHarnessNine.setInServiceDate("11.07.2017");
        fullBodyHarnessNine.setExpirationDate("11.07.2022");
        fullBodyHarnessNine.setNextPeriodicInspectionDate("11.07.2018");
        fullBodyHarnessNine.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm fullBodyHarnessTen = new Ipm();
        fullBodyHarnessTen.setId(0);
        fullBodyHarnessTen.setName("Страховочная привязь");
        fullBodyHarnessTen.setManufacturer("Vento");
        fullBodyHarnessTen.setSerialNumber("1312007313");
        fullBodyHarnessTen.setReleaseDate("01.04.2017");
        fullBodyHarnessTen.setInServiceDate("25.07.2017");
        fullBodyHarnessTen.setExpirationDate("25.07.2022");
        fullBodyHarnessTen.setNextPeriodicInspectionDate("25.07.2018");
        fullBodyHarnessTen.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm insulatingGlovesOne = new Ipm();
        insulatingGlovesOne.setId(0);
        insulatingGlovesOne.setName("Диэлектрические перчатки");
        insulatingGlovesOne.setManufacturer("ЮгСпецЗащита");
        insulatingGlovesOne.setSerialNumber("Отсутствует");
        insulatingGlovesOne.setReleaseDate("01.11.2016");
        insulatingGlovesOne.setInServiceDate("01.12.2016");
        insulatingGlovesOne.setExpirationDate("До появления первого дефекта, либо по решению проверяющей комиссии");
        insulatingGlovesOne.setNextPeriodicInspectionDate("01.05.2018");
        insulatingGlovesOne.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm insulatingGlovesTwo = new Ipm();
        insulatingGlovesTwo.setId(0);
        insulatingGlovesTwo.setName("Диэлектрические перчатки");
        insulatingGlovesTwo.setManufacturer("ЮгСпецЗащита");
        insulatingGlovesTwo.setSerialNumber("Отсутствует");
        insulatingGlovesTwo.setReleaseDate("01.11.2016");
        insulatingGlovesTwo.setInServiceDate("01.11.2017");
        insulatingGlovesTwo.setExpirationDate("До появления первого дефекта, либо по решению проверяющей комиссии");
        insulatingGlovesTwo.setNextPeriodicInspectionDate("01.05.2018");
        insulatingGlovesTwo.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm insulatingGlovesThree = new Ipm();
        insulatingGlovesThree.setId(0);
        insulatingGlovesThree.setName("Диэлектрические перчатки");
        insulatingGlovesThree.setManufacturer("ЮгСпецЗащита");
        insulatingGlovesThree.setSerialNumber("Отсутствует");
        insulatingGlovesThree.setReleaseDate("01.11.2016");
        insulatingGlovesThree.setInServiceDate("01.12.2016");
        insulatingGlovesThree.setExpirationDate("До появления первого дефекта, либо по решению проверяющей комиссии");
        insulatingGlovesThree.setNextPeriodicInspectionDate("01.05.2018");
        insulatingGlovesThree.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm insulatingGlovesFour = new Ipm();
        insulatingGlovesFour.setId(0);
        insulatingGlovesFour.setName("Диэлектрические перчатки");
        insulatingGlovesFour.setManufacturer("ЮгСпецЗащита");
        insulatingGlovesFour.setSerialNumber("Отсутствует");
        insulatingGlovesFour.setReleaseDate("01.11.2016");
        insulatingGlovesFour.setInServiceDate("01.12.2016");
        insulatingGlovesFour.setExpirationDate("До появления первого дефекта, либо по решению проверяющей комиссии");
        insulatingGlovesFour.setNextPeriodicInspectionDate("01.05.2018");
        insulatingGlovesFour.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm insulatingGlovesFive = new Ipm();
        insulatingGlovesFive.setId(0);
        insulatingGlovesFive.setName("Диэлектрические перчатки");
        insulatingGlovesFive.setManufacturer("ЮгСпецЗащита");
        insulatingGlovesFive.setSerialNumber("Отсутствует");
        insulatingGlovesFive.setReleaseDate("01.11.2016");
        insulatingGlovesFive.setInServiceDate("01.12.2016");
        insulatingGlovesFive.setExpirationDate("До появления первого дефекта, либо по решению проверяющей комиссии");
        insulatingGlovesFive.setNextPeriodicInspectionDate("01.05.2018");
        insulatingGlovesFive.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm insulatingGlovesSix = new Ipm();
        insulatingGlovesSix.setId(0);
        insulatingGlovesSix.setName("Диэлектрические перчатки");
        insulatingGlovesSix.setManufacturer("ЮгСпецЗащита");
        insulatingGlovesSix.setSerialNumber("Отсутствует");
        insulatingGlovesSix.setReleaseDate("01.11.2016");
        insulatingGlovesSix.setInServiceDate("01.12.2016");
        insulatingGlovesSix.setExpirationDate("До появления первого дефекта, либо по решению проверяющей комиссии");
        insulatingGlovesSix.setNextPeriodicInspectionDate("01.05.2018");
        insulatingGlovesSix.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm insulatingGlovesSeven = new Ipm();
        insulatingGlovesSeven.setId(0);
        insulatingGlovesSeven.setName("Диэлектрические перчатки");
        insulatingGlovesSeven.setManufacturer("ЮгСпецЗащита");
        insulatingGlovesSeven.setSerialNumber("Отсутствует");
        insulatingGlovesSeven.setReleaseDate("01.11.2016");
        insulatingGlovesSeven.setInServiceDate("01.12.2016");
        insulatingGlovesSeven.setExpirationDate("До появления первого дефекта, либо по решению проверяющей комиссии");
        insulatingGlovesSeven.setNextPeriodicInspectionDate("01.05.2018");
        insulatingGlovesSeven.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm insulatingGlovesEight = new Ipm();
        insulatingGlovesEight.setId(0);
        insulatingGlovesEight.setName("Диэлектрические перчатки");
        insulatingGlovesEight.setManufacturer("ЮгСпецЗащита");
        insulatingGlovesEight.setSerialNumber("Отсутствует");
        insulatingGlovesEight.setReleaseDate("01.11.2016");
        insulatingGlovesEight.setInServiceDate("01.12.2016");
        insulatingGlovesEight.setExpirationDate("До появления первого дефекта, либо по решению проверяющей комиссии");
        insulatingGlovesEight.setNextPeriodicInspectionDate("01.05.2018");
        insulatingGlovesEight.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm insulatingGlovesNine = new Ipm();
        insulatingGlovesNine.setId(0);
        insulatingGlovesNine.setName("Диэлектрические перчатки");
        insulatingGlovesNine.setManufacturer("ЮгСпецЗащита");
        insulatingGlovesNine.setSerialNumber("Отсутствует");
        insulatingGlovesNine.setReleaseDate("01.11.2016");
        insulatingGlovesNine.setInServiceDate("01.12.2016");
        insulatingGlovesNine.setExpirationDate("До появления первого дефекта, либо по решению проверяющей комиссии");
        insulatingGlovesNine.setNextPeriodicInspectionDate("01.05.2018");
        insulatingGlovesNine.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm insulatingGlovesTen = new Ipm();
        insulatingGlovesTen.setId(0);
        insulatingGlovesTen.setName("Диэлектрические перчатки");
        insulatingGlovesTen.setManufacturer("ЮгСпецЗащита");
        insulatingGlovesTen.setSerialNumber("Отсутствует");
        insulatingGlovesTen.setReleaseDate("01.11.2016");
        insulatingGlovesTen.setInServiceDate("01.12.2016");
        insulatingGlovesTen.setExpirationDate("До появления первого дефекта, либо по решению проверяющей комиссии");
        insulatingGlovesTen.setNextPeriodicInspectionDate("01.05.2018");
        insulatingGlovesTen.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm voltageDetectorOne = new Ipm();
        voltageDetectorOne.setId(0);
        voltageDetectorOne.setName("Указатель напряжения");
        voltageDetectorOne.setManufacturer("ЭлектроТрэйд");
        voltageDetectorOne.setSerialNumber("3417");
        voltageDetectorOne.setReleaseDate("01.03.2017");
        voltageDetectorOne.setInServiceDate("01.05.2017");
        voltageDetectorOne.setExpirationDate("До появления дефекта, либо по решению проверяющей комиссии");
        voltageDetectorOne.setNextPeriodicInspectionDate("01.05.2018");
        voltageDetectorOne.setUsage("Необходимо проверить целостность, отсутствие загрязнений, дату последнего испытания, а также рабочее напряжение");

        Ipm voltageDetectorTwo = new Ipm();
        voltageDetectorTwo.setId(0);
        voltageDetectorTwo.setName("Указатель напряжения");
        voltageDetectorTwo.setManufacturer("ЭлектроТрэйд");
        voltageDetectorTwo.setSerialNumber("3414");
        voltageDetectorTwo.setReleaseDate("01.03.2017");
        voltageDetectorTwo.setInServiceDate("01.05.2017");
        voltageDetectorTwo.setExpirationDate("До появления дефекта, либо по решению проверяющей комиссии");
        voltageDetectorTwo.setNextPeriodicInspectionDate("01.05.2018");
        voltageDetectorTwo.setUsage("Необходимо проверить целостность, отсутствие загрязнений, дату последнего испытания, а также рабочее напряжение");

        Ipm voltageDetectorThree = new Ipm();
        voltageDetectorThree.setId(0);
        voltageDetectorThree.setName("Указатель напряжения");
        voltageDetectorThree.setManufacturer("ЭлектроТрэйд");
        voltageDetectorThree.setSerialNumber("3421");
        voltageDetectorThree.setReleaseDate("01.03.2017");
        voltageDetectorThree.setInServiceDate("01.05.2017");
        voltageDetectorThree.setExpirationDate("До появления дефекта, либо по решению проверяющей комиссии");
        voltageDetectorThree.setNextPeriodicInspectionDate("01.05.2018");
        voltageDetectorThree.setUsage("Необходимо проверить целостность, отсутствие загрязнений, дату последнего испытания, а также рабочее напряжение");

        Ipm voltageDetectorFour = new Ipm();
        voltageDetectorFour.setId(0);
        voltageDetectorFour.setName("Указатель напряжения");
        voltageDetectorFour.setManufacturer("ЭлектроТрэйд");
        voltageDetectorFour.setSerialNumber("3425");
        voltageDetectorFour.setReleaseDate("01.03.2017");
        voltageDetectorFour.setInServiceDate("01.05.2017");
        voltageDetectorFour.setExpirationDate("До появления дефекта, либо по решению проверяющей комиссии");
        voltageDetectorFour.setNextPeriodicInspectionDate("01.05.2018");
        voltageDetectorFour.setUsage("Необходимо проверить целостность, отсутствие загрязнений, дату последнего испытания, а также рабочее напряжение");

        Ipm voltageDetectorFive = new Ipm();
        voltageDetectorFive.setId(0);
        voltageDetectorFive.setName("Указатель напряжения");
        voltageDetectorFive.setManufacturer("ЭлектроТрэйд");
        voltageDetectorFive.setSerialNumber("3426");
        voltageDetectorFive.setReleaseDate("01.03.2017");
        voltageDetectorFive.setInServiceDate("01.05.2017");
        voltageDetectorFive.setExpirationDate("До появления дефекта, либо по решению проверяющей комиссии");
        voltageDetectorFive.setNextPeriodicInspectionDate("01.05.2018");
        voltageDetectorFive.setUsage("Необходимо проверить целостность, отсутствие загрязнений, дату последнего испытания, а также рабочее напряжение");

        Ipm voltageDetectorSix = new Ipm();
        voltageDetectorSix.setId(0);
        voltageDetectorSix.setName("Указатель напряжения");
        voltageDetectorSix.setManufacturer("ЭлектроТрэйд");
        voltageDetectorSix.setSerialNumber("3429");
        voltageDetectorSix.setReleaseDate("01.03.2017");
        voltageDetectorSix.setInServiceDate("01.05.2017");
        voltageDetectorSix.setExpirationDate("До появления дефекта, либо по решению проверяющей комиссии");
        voltageDetectorSix.setNextPeriodicInspectionDate("01.05.2018");
        voltageDetectorSix.setUsage("Необходимо проверить целостность, отсутствие загрязнений, дату последнего испытания, а также рабочее напряжение");

        Ipm voltageDetectorSeven = new Ipm();
        voltageDetectorSeven.setId(0);
        voltageDetectorSeven.setName("Указатель напряжения");
        voltageDetectorSeven.setManufacturer("ЭлектроТрэйд");
        voltageDetectorSeven.setSerialNumber("3433");
        voltageDetectorSeven.setReleaseDate("01.03.2017");
        voltageDetectorSeven.setInServiceDate("01.05.2017");
        voltageDetectorSeven.setExpirationDate("До появления дефекта, либо по решению проверяющей комиссии");
        voltageDetectorSeven.setNextPeriodicInspectionDate("01.05.2018");
        voltageDetectorSeven.setUsage("Необходимо проверить целостность, отсутствие загрязнений, дату последнего испытания, а также рабочее напряжение");

        Ipm voltageDetectorEight = new Ipm();
        voltageDetectorEight.setId(0);
        voltageDetectorEight.setName("Указатель напряжения");
        voltageDetectorEight.setManufacturer("ЭлектроТрэйд");
        voltageDetectorEight.setSerialNumber("3436");
        voltageDetectorEight.setReleaseDate("01.03.2017");
        voltageDetectorEight.setInServiceDate("01.05.2017");
        voltageDetectorEight.setExpirationDate("До появления дефекта, либо по решению проверяющей комиссии");
        voltageDetectorEight.setNextPeriodicInspectionDate("01.05.2018");
        voltageDetectorEight.setUsage("Необходимо проверить целостность, отсутствие загрязнений, дату последнего испытания, а также рабочее напряжение");

        Ipm voltageDetectorNine = new Ipm();
        voltageDetectorNine.setId(0);
        voltageDetectorNine.setName("Указатель напряжения");
        voltageDetectorNine.setManufacturer("ЭлектроТрэйд");
        voltageDetectorNine.setSerialNumber("3439");
        voltageDetectorNine.setReleaseDate("01.03.2017");
        voltageDetectorNine.setInServiceDate("01.05.2017");
        voltageDetectorNine.setExpirationDate("До появления дефекта, либо по решению проверяющей комиссии");
        voltageDetectorNine.setNextPeriodicInspectionDate("01.05.2018");
        voltageDetectorNine.setUsage("Необходимо проверить целостность, отсутствие загрязнений, дату последнего испытания, а также рабочее напряжение");

        Ipm voltageDetectorTen = new Ipm();
        voltageDetectorTen.setId(0);
        voltageDetectorTen.setName("Указатель напряжения");
        voltageDetectorTen.setManufacturer("ЭлектроТрэйд");
        voltageDetectorTen.setSerialNumber("3444");
        voltageDetectorTen.setReleaseDate("01.03.2017");
        voltageDetectorTen.setInServiceDate("01.05.2017");
        voltageDetectorTen.setExpirationDate("До появления дефекта, либо по решению проверяющей комиссии");
        voltageDetectorTen.setNextPeriodicInspectionDate("01.05.2018");
        voltageDetectorTen.setUsage("Необходимо проверить целостность, отсутствие загрязнений, дату последнего испытания, а также рабочее напряжение");

        Ipm dielectricScrewdriverOne = new Ipm();
        dielectricScrewdriverOne.setId(0);
        dielectricScrewdriverOne.setName("Набор диэлектрических отверток");
        dielectricScrewdriverOne.setManufacturer("Dexter");
        dielectricScrewdriverOne.setSerialNumber("Отсутствует");
        dielectricScrewdriverOne.setReleaseDate("01.09.2017");
        dielectricScrewdriverOne.setInServiceDate("01.10.2017");
        dielectricScrewdriverOne.setExpirationDate("До появления дефекта, либо по решению проверяющей комиссии");
        dielectricScrewdriverOne.setNextPeriodicInspectionDate("01.10.2018");
        dielectricScrewdriverOne.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm dielectricScrewdriverTwo = new Ipm();
        dielectricScrewdriverTwo.setId(0);
        dielectricScrewdriverTwo.setName("Набор диэлектрических отверток");
        dielectricScrewdriverTwo.setManufacturer("Dexter");
        dielectricScrewdriverTwo.setSerialNumber("Отсутствует");
        dielectricScrewdriverTwo.setReleaseDate("01.09.2017");
        dielectricScrewdriverTwo.setInServiceDate("01.10.2017");
        dielectricScrewdriverTwo.setExpirationDate("До появления дефекта, либо по решению проверяющей комиссии");
        dielectricScrewdriverTwo.setNextPeriodicInspectionDate("01.10.2018");
        dielectricScrewdriverTwo.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm dielectricScrewdriverThree = new Ipm();
        dielectricScrewdriverThree.setId(0);
        dielectricScrewdriverThree.setName("Набор диэлектрических отверток");
        dielectricScrewdriverThree.setManufacturer("Dexter");
        dielectricScrewdriverThree.setSerialNumber("Отсутствует");
        dielectricScrewdriverThree.setReleaseDate("01.09.2017");
        dielectricScrewdriverThree.setInServiceDate("01.10.2017");
        dielectricScrewdriverThree.setExpirationDate("До появления дефекта, либо по решению проверяющей комиссии");
        dielectricScrewdriverThree.setNextPeriodicInspectionDate("01.10.2018");
        dielectricScrewdriverThree.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm dielectricScrewdriverFour = new Ipm();
        dielectricScrewdriverFour.setId(0);
        dielectricScrewdriverFour.setName("Набор диэлектрических отверток");
        dielectricScrewdriverFour.setManufacturer("Dexter");
        dielectricScrewdriverFour.setSerialNumber("Отсутствует");
        dielectricScrewdriverFour.setReleaseDate("01.09.2017");
        dielectricScrewdriverFour.setInServiceDate("01.10.2017");
        dielectricScrewdriverFour.setExpirationDate("До появления дефекта, либо по решению проверяющей комиссии");
        dielectricScrewdriverFour.setNextPeriodicInspectionDate("01.10.2018");
        dielectricScrewdriverFour.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm dielectricScrewdriverFive = new Ipm();
        dielectricScrewdriverFive.setId(0);
        dielectricScrewdriverFive.setName("Набор диэлектрических отверток");
        dielectricScrewdriverFive.setManufacturer("Dexter");
        dielectricScrewdriverFive.setSerialNumber("Отсутствует");
        dielectricScrewdriverFive.setReleaseDate("01.09.2017");
        dielectricScrewdriverFive.setInServiceDate("01.10.2017");
        dielectricScrewdriverFive.setExpirationDate("До появления дефекта, либо по решению проверяющей комиссии");
        dielectricScrewdriverFive.setNextPeriodicInspectionDate("01.10.2018");
        dielectricScrewdriverFive.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm dielectricScrewdriverSix = new Ipm();
        dielectricScrewdriverSix.setId(0);
        dielectricScrewdriverSix.setName("Набор диэлектрических отверток");
        dielectricScrewdriverSix.setManufacturer("Dexter");
        dielectricScrewdriverSix.setSerialNumber("Отсутствует");
        dielectricScrewdriverSix.setReleaseDate("01.09.2017");
        dielectricScrewdriverSix.setInServiceDate("01.10.2017");
        dielectricScrewdriverSix.setExpirationDate("До появления дефекта, либо по решению проверяющей комиссии");
        dielectricScrewdriverSix.setNextPeriodicInspectionDate("01.10.2018");
        dielectricScrewdriverSix.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm dielectricScrewdriverSeven = new Ipm();
        dielectricScrewdriverSeven.setId(0);
        dielectricScrewdriverSeven.setName("Набор диэлектрических отверток");
        dielectricScrewdriverSeven.setManufacturer("Dexter");
        dielectricScrewdriverSeven.setSerialNumber("Отсутствует");
        dielectricScrewdriverSeven.setReleaseDate("01.09.2017");
        dielectricScrewdriverSeven.setInServiceDate("01.10.2017");
        dielectricScrewdriverSeven.setExpirationDate("До появления дефекта, либо по решению проверяющей комиссии");
        dielectricScrewdriverSeven.setNextPeriodicInspectionDate("01.10.2018");
        dielectricScrewdriverSeven.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm dielectricScrewdriverEight = new Ipm();
        dielectricScrewdriverEight.setId(0);
        dielectricScrewdriverEight.setName("Набор диэлектрических отверток");
        dielectricScrewdriverEight.setManufacturer("Dexter");
        dielectricScrewdriverEight.setSerialNumber("Отсутствует");
        dielectricScrewdriverEight.setReleaseDate("01.09.2017");
        dielectricScrewdriverEight.setInServiceDate("01.10.2017");
        dielectricScrewdriverEight.setExpirationDate("До появления дефекта, либо по решению проверяющей комиссии");
        dielectricScrewdriverEight.setNextPeriodicInspectionDate("01.10.2018");
        dielectricScrewdriverEight.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm dielectricScrewdriverNine = new Ipm();
        dielectricScrewdriverNine.setId(0);
        dielectricScrewdriverNine.setName("Набор диэлектрических отверток");
        dielectricScrewdriverNine.setManufacturer("Dexter");
        dielectricScrewdriverNine.setSerialNumber("Отсутствует");
        dielectricScrewdriverNine.setReleaseDate("01.09.2017");
        dielectricScrewdriverNine.setInServiceDate("01.10.2017");
        dielectricScrewdriverNine.setExpirationDate("До появления дефекта, либо по решению проверяющей комиссии");
        dielectricScrewdriverNine.setNextPeriodicInspectionDate("01.10.2018");
        dielectricScrewdriverNine.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Ipm dielectricScrewdriverTen = new Ipm();
        dielectricScrewdriverTen.setId(0);
        dielectricScrewdriverTen.setName("Набор диэлектрических отверток");
        dielectricScrewdriverTen.setManufacturer("Dexter");
        dielectricScrewdriverTen.setSerialNumber("Отсутствует");
        dielectricScrewdriverTen.setReleaseDate("01.09.2017");
        dielectricScrewdriverTen.setInServiceDate("01.10.2017");
        dielectricScrewdriverTen.setExpirationDate("До появления дефекта, либо по решению проверяющей комиссии");
        dielectricScrewdriverTen.setNextPeriodicInspectionDate("01.10.2018");
        dielectricScrewdriverTen.setUsage("Визуальный осмотр перед каждым использованием. При выявлении дефектов использовать запрещено. Передать ответственному за СИЗ сотруднику");

        Worker hurshed = new Worker();
        hurshed.setId(0);
        hurshed.setName("Авганов Хуршед");
        fullBodyHarnessOne.setWorker(hurshed);
        insulatingGlovesOne.setWorker(hurshed);
        voltageDetectorOne.setWorker(hurshed);
        dielectricScrewdriverOne.setWorker(hurshed);
        hurshed.setIpms(List.of(fullBodyHarnessOne, insulatingGlovesOne, voltageDetectorOne, dielectricScrewdriverOne));
        workersRepository.save(hurshed);
        ipmsRepository.save(fullBodyHarnessOne);
        ipmsRepository.save(insulatingGlovesOne);
        ipmsRepository.save(voltageDetectorOne);
        ipmsRepository.save(dielectricScrewdriverOne);

        Worker ildar = new Worker();
        ildar.setId(0);
        ildar.setName("Губайдуллин Ильдар");
        fullBodyHarnessThree.setWorker(ildar);
        insulatingGlovesThree.setWorker(ildar);
        voltageDetectorThree.setWorker(ildar);
        dielectricScrewdriverThree.setWorker(ildar);
        ildar.setIpms(List.of(fullBodyHarnessThree, insulatingGlovesThree, voltageDetectorThree, dielectricScrewdriverThree));
        workersRepository.save(ildar);
        ipmsRepository.save(fullBodyHarnessThree);
        ipmsRepository.save(insulatingGlovesThree);
        ipmsRepository.save(voltageDetectorThree);
        ipmsRepository.save(dielectricScrewdriverThree);

        Worker ilnur = new Worker();
        ilnur.setId(0);
        ilnur.setName("Ибрагимов Ильнур");
        fullBodyHarnessTwo.setWorker(ilnur);
        insulatingGlovesTwo.setWorker(ilnur);
        voltageDetectorTwo.setWorker(ilnur);
        dielectricScrewdriverTwo.setWorker(ilnur);
        ilnur.setIpms(List.of(fullBodyHarnessTwo, insulatingGlovesTwo, voltageDetectorTwo, dielectricScrewdriverTwo));
        workersRepository.save(ilnur);
        ipmsRepository.save(fullBodyHarnessTwo);
        ipmsRepository.save(insulatingGlovesTwo);
        ipmsRepository.save(voltageDetectorTwo);
        ipmsRepository.save(dielectricScrewdriverTwo);

        Worker viktor = new Worker();
        viktor.setId(0);
        viktor.setName("Кузнецов Виктор");
        fullBodyHarnessFour.setWorker(viktor);
        insulatingGlovesFour.setWorker(viktor);
        voltageDetectorFour.setWorker(viktor);
        dielectricScrewdriverFour.setWorker(viktor);
        viktor.setIpms(List.of(fullBodyHarnessFour, insulatingGlovesFour, voltageDetectorFour, dielectricScrewdriverFour));
        workersRepository.save(viktor);
        ipmsRepository.save(fullBodyHarnessFour);
        ipmsRepository.save(insulatingGlovesFour);
        ipmsRepository.save(voltageDetectorFour);
        ipmsRepository.save(dielectricScrewdriverFour);

        Worker dmitriy = new Worker();
        dmitriy.setId(0);
        dmitriy.setName("Николаев Дмитрий");
        fullBodyHarnessFive.setWorker(dmitriy);
        insulatingGlovesFive.setWorker(dmitriy);
        voltageDetectorFive.setWorker(dmitriy);
        dielectricScrewdriverFive.setWorker(dmitriy);
        dmitriy.setIpms(List.of(fullBodyHarnessFive, insulatingGlovesFive, voltageDetectorFive, dielectricScrewdriverFive));
        workersRepository.save(dmitriy);
        ipmsRepository.save(fullBodyHarnessFive);
        ipmsRepository.save(insulatingGlovesFive);
        ipmsRepository.save(voltageDetectorFive);
        ipmsRepository.save(dielectricScrewdriverFive);

        Worker andrey = new Worker();
        andrey.setId(0);
        andrey.setName("Панцырев Андрей");
        fullBodyHarnessSix.setWorker(andrey);
        insulatingGlovesSix.setWorker(andrey);
        voltageDetectorSix.setWorker(andrey);
        dielectricScrewdriverSix.setWorker(andrey);
        andrey.setIpms(List.of(fullBodyHarnessSix, insulatingGlovesSix, voltageDetectorSix, dielectricScrewdriverSix));
        workersRepository.save(andrey);
        ipmsRepository.save(fullBodyHarnessSix);
        ipmsRepository.save(insulatingGlovesSix);
        ipmsRepository.save(voltageDetectorSix);
        ipmsRepository.save(dielectricScrewdriverSix);

        Worker sergey = new Worker();
        sergey.setId(0);
        sergey.setName("Романов Сергей");
        fullBodyHarnessSeven.setWorker(sergey);
        insulatingGlovesSeven.setWorker(sergey);
        voltageDetectorSeven.setWorker(sergey);
        dielectricScrewdriverSeven.setWorker(sergey);
        sergey.setIpms(List.of(fullBodyHarnessSeven, insulatingGlovesSeven, voltageDetectorSeven, dielectricScrewdriverSeven));
        workersRepository.save(sergey);
        ipmsRepository.save(fullBodyHarnessSeven);
        ipmsRepository.save(insulatingGlovesSeven);
        ipmsRepository.save(voltageDetectorSeven);
        ipmsRepository.save(dielectricScrewdriverSeven);

        Worker renat = new Worker();
        renat.setId(0);
        renat.setName("Саетгараев Ренат");
        fullBodyHarnessEight.setWorker(renat);
        insulatingGlovesEight.setWorker(renat);
        voltageDetectorEight.setWorker(renat);
        dielectricScrewdriverEight.setWorker(renat);
        renat.setIpms(List.of(fullBodyHarnessEight, insulatingGlovesEight, voltageDetectorEight, dielectricScrewdriverEight));
        workersRepository.save(renat);
        ipmsRepository.save(fullBodyHarnessEight);
        ipmsRepository.save(insulatingGlovesEight);
        ipmsRepository.save(voltageDetectorEight);
        ipmsRepository.save(dielectricScrewdriverEight);

        Worker lenar = new Worker();
        lenar.setId(0);
        lenar.setName("Хайруллин Ленар");
        fullBodyHarnessNine.setWorker(lenar);
        insulatingGlovesNine.setWorker(lenar);
        voltageDetectorNine.setWorker(lenar);
        dielectricScrewdriverNine.setWorker(lenar);
        lenar.setIpms(List.of(fullBodyHarnessNine, insulatingGlovesNine, voltageDetectorNine, dielectricScrewdriverNine));
        workersRepository.save(lenar);
        ipmsRepository.save(fullBodyHarnessNine);
        ipmsRepository.save(insulatingGlovesNine);
        ipmsRepository.save(voltageDetectorNine);
        ipmsRepository.save(dielectricScrewdriverNine);

        Worker artur = new Worker();
        artur.setId(0);
        artur.setName("Хафизов Артур");
        fullBodyHarnessTen.setWorker(artur);
        insulatingGlovesTen.setWorker(artur);
        voltageDetectorTen.setWorker(artur);
        dielectricScrewdriverTen.setWorker(artur);
        artur.setIpms(List.of(fullBodyHarnessTen, insulatingGlovesTen, voltageDetectorTen, dielectricScrewdriverTen));
        workersRepository.save(artur);
        ipmsRepository.save(fullBodyHarnessTen);
        ipmsRepository.save(insulatingGlovesTen);
        ipmsRepository.save(voltageDetectorTen);
        ipmsRepository.save(dielectricScrewdriverTen);
    }
}