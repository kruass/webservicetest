package ru.itpark.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itpark.entity.Account;

@Repository
public interface AccountsRepository extends JpaRepository<Account, Integer> {
    Account findByUsername(String username);
}
