package ru.itpark.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itpark.entity.Worker;

import java.util.List;
@Repository
public interface WorkersRepository extends JpaRepository<Worker, Integer> {
    List<Worker> findAllByNameContainsIgnoreCase(String name);
}

