package ru.itpark.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.itpark.entity.Ipm;
import ru.itpark.entity.Worker;

import java.util.List;

@Repository
public interface IpmsRepository extends JpaRepository<Ipm,Integer> {

    List<Ipm> findAllByNameContainsIgnoreCase(String name);
    List<Ipm> findAllByWorker_Id(int id);

}
