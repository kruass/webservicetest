package ru.itpark;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.itpark.entity.Account;
import ru.itpark.repository.AccountsRepository;
import ru.itpark.service.WorkersService;

import java.util.List;

@SpringBootApplication
public class WebservicetestApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(WebservicetestApplication.class, args);

		context.getBean(WorkersService.class).service();
		PasswordEncoder passwordEncoder = context.getBean(PasswordEncoder.class);
		AccountsRepository accountsRepository = context.getBean(AccountsRepository.class);

		accountsRepository.save(new Account(
				0,
				"admin",
				passwordEncoder.encode("password"),
				List.of(
						new SimpleGrantedAuthority("VIEW"),
						new SimpleGrantedAuthority("EDIT"),
						new SimpleGrantedAuthority("REMOVE")
				),
				true,
				true,
				true,
				true
		));
		accountsRepository.save(new Account(
				0,
				"user",
				passwordEncoder.encode("password"),
				List.of(
						new SimpleGrantedAuthority("VIEW")
				),
				true,
				true,
				true,
				true
		));
	}
}
