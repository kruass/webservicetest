package ru.itpark.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import ru.itpark.entity.Account;
import ru.itpark.entity.Ipm;
import ru.itpark.entity.Worker;
import ru.itpark.service.WorkersService;


import java.util.List;

@Controller
@RequestMapping("/workers")
public class WorkersController {
    private final WorkersService workersService;

    @Autowired
    public WorkersController(WorkersService workersService) {
        this.workersService = workersService;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('VIEW')")
    public String findAll(Model model, @AuthenticationPrincipal Account account) {
        model.addAttribute("workers", workersService.findAllWorker());
        model.addAttribute("account", account);
        return "workers";

    }

    @GetMapping("/add")
    @PreAuthorize("hasAuthority('EDIT')")
    public String addWorkerForm() {
        return "worker-add";
    }

    @PostMapping("/add")
    @PreAuthorize("hasAuthority('EDIT')")
    public String addWorker(@ModelAttribute Worker worker) {
        workersService.addWorker(worker);
        return "redirect:/workers";
    }

    @GetMapping("/{id}/add")
    @PreAuthorize("hasAuthority('EDIT')")
    public String addIpmForm(@PathVariable int id, Model model) {
        model.addAttribute("ipm", new Ipm());
        model.addAttribute("worker", workersService.findWorkerById(id));
        return "ipm-add";
    }

    @PostMapping("/{id}/add")
    @PreAuthorize("hasAuthority('EDIT')")
    public String addIpmToWorker(@PathVariable int id,@ModelAttribute Ipm ipm) {
        Worker worker = workersService.findWorkerById(id);
        List<Ipm> ipmsOfWorker = workersService.searchIpmByWorkerId(id);
        ipmsOfWorker.add(ipm);
        ipm.setWorker(worker);
        workersService.addIpm(ipm);
        workersService.addWorker(workersService.findWorkerById(id));

        return "redirect:/workers/{id}";
    }


    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('VIEW')")
    public String searchIpmByWorkerId(@PathVariable int id, Model model, @AuthenticationPrincipal Account account){
        model.addAttribute("ipmByWorkerId", workersService.searchIpmByWorkerId(id));
        model.addAttribute("workerId", id);
        model.addAttribute("workerName", workersService.findWorkerById(id).getName());
        model.addAttribute("account", account);
        return "worker";
    }

    @GetMapping("/{id}/{ipmId}")
    @PreAuthorize("hasAuthority('VIEW')")
    public String get(@PathVariable (value = "id") int id,@PathVariable (value = "ipmId") int ipmId, Model model, @AuthenticationPrincipal Account account) {
        model.addAttribute("ipm", workersService.findIpmById(ipmId));
        model.addAttribute("ipmId", ipmId);
        model.addAttribute("workerId", id);
        model.addAttribute("account", account);
        return "ipm";
    }


    @PostMapping("/{id}/{ipmId}/remove")
    @PreAuthorize("hasAuthority('REMOVE')")
    public String removeIpm(@PathVariable (value = "id") int id, @PathVariable (value = "ipmId") int ipmId) {
        workersService.removeIpmById(ipmId);
        return "redirect:/workers/{id}";
    }

    @PostMapping("/{id}/remove")
    @PreAuthorize("hasAuthority('REMOVE')")
    public String removeWorker(@PathVariable int id) {
        workersService.removeWorkerById(id);
        return "redirect:/workers";
    }
}