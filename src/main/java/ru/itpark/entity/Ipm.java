package ru.itpark.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Ipm{
    @Id
    @GeneratedValue
    private int id;
    private String name;
    private String manufacturer;
    private String serialNumber;
    private String releaseDate;
    private String inServiceDate;
    private String expirationDate;
    private String nextPeriodicInspectionDate;
    private String usage;
    @ManyToOne(cascade = CascadeType.PERSIST) // у многих клиентов один менеджер
    private Worker worker;
}
